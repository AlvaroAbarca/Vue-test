Vue.component('tv-show',{
    props: {
        name: String,
        season: Number,
    },
    template: `<li>{{ name }} <strong>{{ season }}</strong> temporadas</li>`
})

const app = new Vue({
    el: '#app',
    data: {
        mostrar: true,
        message: 'Hola VueJS ' + new Date().toLocaleString(),
        tvshows: [
            { name: 'Game of Thrones', season: 7 },
            { name: 'Breaking Bad', season: 5 },
            { name: 'LOST', season: 6 },
            { name: 'BattleStar Galactia', season: 4 },
        ]
    },
    methods: {
        toogleMostrar() {
            this.mostrar = !this.mostrar
        },
        reverseMessage: function () {
            this.message = this.message.split('').reverse().join('')
        }
    }
})